<?php require_once('Connections/localhost.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmpasswd")) {
  $updateSQL = sprintf("UPDATE `user` SET pwd=%s, email=%s, random_var=%s WHERE `uid`=%s",
                       GetSQLValueString(hash('sha256',$_POST['pwd']), "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['btgenerate'], "text"),
                       GetSQLValueString($_POST['uid'], "text"));

  mysql_select_db($database_localhost, $localhost);
  $Result1 = mysql_query($updateSQL, $localhost) or die(mysql_error());

  $updateGoTo = "setmsg.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset_edit = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_Recordset_edit = $_SESSION['MM_Username'];
}
mysql_select_db($database_localhost, $localhost);
$query_Recordset_edit = sprintf("SELECT * FROM `user` WHERE `uid` = %s", GetSQLValueString($colname_Recordset_edit, "text"));
$Recordset_edit = mysql_query($query_Recordset_edit, $localhost) or die(mysql_error());
$row_Recordset_edit = mysql_fetch_assoc($Recordset_edit);
$totalRows_Recordset_edit = mysql_num_rows($Recordset_edit);$colname_Recordset_edit = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_Recordset_edit = $_SESSION['MM_Username'];
}
mysql_select_db($database_localhost, $localhost);
$query_Recordset_edit = sprintf("SELECT * FROM `user` WHERE email = %s", GetSQLValueString($colname_Recordset_edit, "text"));
$Recordset_edit = mysql_query($query_Recordset_edit, $localhost) or die(mysql_error());
$row_Recordset_edit = mysql_fetch_assoc($Recordset_edit);
$totalRows_Recordset_edit = mysql_num_rows($Recordset_edit);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title></title>
<link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<script>
function makepasswd(length)
{
    if (length < 8) {alert("強壯密碼的長度最少需要8"); return "";}
    var rets = "";    
    var s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    var i = 0;
    var pos;
    while (i < length)
    {        
        pos = Math.random()*s.length;
        rets = rets + s.substring(pos,pos+1);        
        i = i + 1;
    }    
    return rets;
}
function showpasswd()
{
    var size = document.frmpasswd.txlength.value;
    size++;
    document.frmpasswd.txpasswd.size  = size;
    document.frmpasswd.txpasswd.value = makepasswd(document.frmpasswd.txlength.value);
    return true;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->變更個人資料<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <form id="frmpasswd" name="frmpasswd" method="POST" action="<?php echo $editFormAction; ?>">
	    <table width="100%" border="0">
	      <tr>
	        <td width="26%" align="right">E-mail：</td>
	        <td width="74%"><label for="email"></label>
            <input name="email" type="text" id="email" value="<?php echo $row_Recordset_edit['email']; ?>" /></td>
          </tr>
	      <tr>
	        <td align="right">登入密碼：</td>
	        <td><label for="pwd"></label>
	          <span id="sprypassword1">
              <input name="pwd" type="password" id="pwd" value="<?php echo $row_Recordset_edit['pwd']; ?>" />
            <span class="passwordRequiredMsg">請輸入密碼</span><span class="passwordInvalidStrengthMsg">密碼必須為英文字母小寫加數字</span><span class="passwordMinCharsMsg">密碼至少需8位數</span></span></td>
          </tr>
	      <tr>
	        <td align="right">確認密碼：</td>
	        <td><label for="repwd"></label>
	          <span id="spryconfirm1">
	          <input name="repwd" type="password" id="repwd" />
            <span class="confirmRequiredMsg">請輸入密碼</span><span class="confirmInvalidMsg">密碼不相符</span></span></td>
          </tr>
	      <tr>
	        <td align="right">加密金鑰：</td>
	        <td><label for="txpasswd"></label>
            <input name="txpasswd" type="text" id="txpasswd" value="<?php echo $row_Recordset_edit['random_var']; ?>" size="17" readonly="readonly" />
            <input type="button" name="btgenerate" id="btgenerate" value="產生密鑰" onclick="showpasswd()"/></td>
	      </tr>
	      <tr>
	        <td align="right"><input name="txlength" type="hidden" id="txlength" value="16" />	          <input name="uid" type="hidden" id="uid" value="<?php echo $row_Recordset_edit['uid']; ?>" /></td>
	        <td><input type="submit" name="submit" id="submit" value="更新資料" /></td>
          </tr>
	    </table>
	    <input type="hidden" name="MM_update" value="frmpasswd" />

    </form>
    <script type="text/javascript">
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "pwd");
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {minAlphaChars:1, minNumbers:1, minChars:8});
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($Recordset_edit);
?>
