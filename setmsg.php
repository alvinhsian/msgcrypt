<?php require_once('Connections/localhost.php'); ?>
<?php require_once('Connections/localhost.php'); 
//require_once('classes/AES.class.php');
$ip = $_SERVER["REMOTE_ADDR"];
//$Cipher = new AES(AES::AES256);
$date_time=date("Y-m-d");
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}

$colname_Recordset1 = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_Recordset1 = $_SESSION['MM_Username'];
}
mysql_select_db($database_localhost, $localhost);
$query_Recordset1 = sprintf("SELECT `uid`, email, random_var FROM `user` WHERE `email` = %s", GetSQLValueString($colname_Recordset1, "text"));
$Recordset1 = mysql_query($query_Recordset1, $localhost) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
//-------------------------------------------------------------以下是加密訊息-----------------------------------------------------------------
$FirstKey = hash('crc32', $row_Recordset1['random_var']);

/*	
mcrypt_generic_init($cipher, $key, $iv);
$decrypted = mdecrypt_generic($cipher,base64_decode($encrypted));
mcrypt_generic_deinit($cipher);
*/	
//echo "encrypted: " . $encrypted;
//echo "\n";
//echo "decrypted: " . substr($decrypted, 0, $length) . "\n";

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	
$SecondKey = hash('crc32', @$_POST['pwd']);
$key=$FirstKey.$SecondKey;//加解密訊息金鑰
$verifyKey_256bit=hash('sha256', @$_POST['pwd'].@$row_Recordset1['random_var'].@$_POST['mid']);//驗證訊息密碼正確性

$cc = @$_POST['m_content'];
$iv = $row_Recordset1['random_var'];
$length = strlen($cc);
	
$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128,'','cbc','');
	
mcrypt_generic_init($cipher, $key, $iv);
$encrypted = base64_encode(mcrypt_generic($cipher,$cc));
mcrypt_generic_deinit($cipher);
$cryptext = $encrypted;
	
    $insertSQL = sprintf("INSERT INTO msgdata (mid, key_1, m_content, m_readed_times, m_read_times_limit, m_show_time, m_end_time, verify_key, m_length) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['mid'], "text"),
                       GetSQLValueString($iv, "text"),
                       GetSQLValueString($cryptext, "text"),
                       GetSQLValueString(0, "text"),
                       GetSQLValueString($_POST['RadioGroup_m_read_times_limit'], "text"),
                       GetSQLValueString($_POST['RadioGroup_m_show_time'], "text"),
                       GetSQLValueString($_POST['m_end_time'], "date"),
					   GetSQLValueString($verifyKey_256bit, "text"),
					   GetSQLValueString($length, "text")
					   );

  mysql_select_db($database_localhost, $localhost);
  $Result1 = mysql_query($insertSQL, $localhost) or die(mysql_error());

  $insertGoTo = "encrypt_ok.php?mid=" . $_POST['mid'];
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}


?>
<?php
function randomKeyGenerator($pt=35, $myword=""){
					$randomKey="";
					$element="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
					$element.= $myword;
					$element_len=strlen($element);
					for($i=1;$i<=$pt;$i++){
						$rg=rand()%$element_len;
						$randomKey.=$element{$rg};
						}
						return $randomKey; 
					}//產生隨機亂數
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>MsgCrypt</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="calander/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calander/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calander/css/steel/steel.css" />
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="calander/js/jscal2.js"></script>
<script type="text/javascript" src="calander/js/lang/tw.js"></script>
<script type="text/javascript" src="jquery/jquery-1.7.2.min.js"></script>
<script language="javascript" type="text/javascript" src="jquery/texteditor/tiny_mce.js"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
		language :"zh-tw",
        theme : "advanced",
        mode : "textareas",
		plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		
		
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
		
		template_external_list_url : "js/template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "js/image_list.js",
        media_external_list_url : "js/media_list.js",
		
		skin : "o2k7",
		skin_variant : "black"
		
});

</script>
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script>
function checkdate(){
	if(document.form1.m_end_time.value=<<? echo $date_time;?>)
		alert("你不能活在過去！");
	}
</script>
<script>
function makepasswd(length)
{
    if (length < 8) {alert("強壯密碼的長度最少需要8"); return "";}
    var rets = "";    
    var s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    var i = 0;
    var pos;
    while (i < length)
    {        
        pos = Math.random()*s.length;
        rets = rets + s.substring(pos,pos+1);        
        i = i + 1;
    }    
    return rets;
}
function showpasswd()
{
    var size = document.form1.txlength.value;
    size++;
    document.form1.pwd.size  = size;
    document.form1.pwd.value = makepasswd(document.form1.txlength.value);
    alert("警告！遺失的密碼無法找回，請務必妥善保存您的密碼！");
	return true;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">登出</a></li>
		<li><a href="#">改版訊息</a></li>
		<li><a href="#">關於作者</a></li>
		<li><a href="#">問與答</a></li>
		<li><a href="#">帳戶資訊</a></li>
	  <!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->歡迎使用<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <form name="form1" id="form1" method="POST" action="<?php echo $editFormAction;?>" >
        
          
          <label for="m_content"></label>
            <div align="center"><span id="sprytextarea1">
              <textarea name="m_content" id="m_content" cols="81" rows="15" ></textarea>
            <span class="textareaRequiredMsg"><br/>
            請輸入內文</span></span></div>
        
	    <p>&nbsp;</p>
        <fieldset>
          <legend>讀取次數限制</legend>
          <p>
            <label>
              <input name="RadioGroup_m_read_times_limit" type="radio" id="RadioGroup_m_read_times_limit_0" value="nolimit" checked="checked" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
              不限制</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="1" id="RadioGroup_m_read_times_limit_1" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
            1次</label>
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="2" id="RadioGroup_m_read_times_limit_2" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
              2次</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="3" id="RadioGroup_m_read_times_limit_3" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
              3次</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="4" id="RadioGroup_m_read_times_limit_4" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
              4次</label>
              
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="5" id="RadioGroup_m_read_times_limit_5" onclick="RadioGroup_m_read_times_limit_6.disabled=true"/>
              5次</label>
          </p>
          <p>
            
            <label>
              <input type="radio" name="RadioGroup_m_read_times_limit" value="" id="custom" onclick="RadioGroup_m_read_times_limit_6.disabled=false"/>
              自訂次數</label>
            <span id="sprytextfield3">
            <label>
              <input name="RadioGroup_m_read_times_limit_" type="text" disabled="disabled" id="RadioGroup_m_read_times_limit_6" value="" size="2" maxlength="2" />
              </label>
          <span class="textfieldInvalidFormatMsg">請輸入整數</span><span class="textfieldRequiredMsg">請指定次數</span></span></p>
        </fieldset>
        <p>&nbsp;</p>
        <fieldset>
          <legend>顯示在畫面上的時間</legend>
          <p>
            <label>
              <input name="RadioGroup_m_show_time" type="radio" id="RadioGroup_m_show_time_0" value="nolimit" checked="checked" />
              不限制</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_show_time" value="30" id="RadioGroup_m_show_time_1" />
              30秒</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_show_time" value="60" id="RadioGroup_m_show_time_2" />
              1分鐘</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_show_time" value="180" id="RadioGroup_m_show_time_3" />
              3分鐘</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_show_time" value="300" id="RadioGroup_m_show_time_4" />
              5分鐘</label>
            
            <label>
              <input type="radio" name="RadioGroup_m_show_time" value="600" id="RadioGroup_m_show_time_5" />
              10分鐘</label>
            
          </p>
        </fieldset>
        <p>&nbsp;</p>
        <fieldset>
          <legend>有效時間/期限</legend>
          <p>
            <label for="m_end_time"></label>
            <span id="sprytextfield2">
            <input name="m_end_time" type="text" id="m_end_time" />
            <span class="textfieldRequiredMsg">請輸入日期</span><span class="textfieldInvalidFormatMsg">格式不正確</span></span><script type="text/javascript">
    			Calendar.setup({
        			trigger    : "m_end_time",
					showTime   :true,
					dateFormat : "%Y-%m-%d",
        			inputField : "m_end_time",
					onSelect   : function() { this.hide() }
   				 });
			</script>
          </p>
        </fieldset>
        <p>&nbsp;</p>
        <fieldset>
          <legend>訊息密碼</legend>
          <p>
            <label for="pwd"></label>
            <span id="sprytextfield1">
            <input name="pwd" type="text" id="pwd" />
            <span class="textfieldRequiredMsg">請輸入密碼</span></span>
          </p>          
            <input type="button" name="btgenerate" id="btgenerate" value="產生密碼" onclick="showpasswd()"/>                     
        </fieldset>       
        <p>
            <div align="right"><input type="submit" name="submit" id="submit" value="我設定好了，請加密訊息！" onclick="checkdate()"/></div>
        </p>
        <input type="hidden" name="MM_insert" value="form1" />
        <input type="hidden" name="mid" id="mid" value="<?php echo randomKeyGenerator();?>"/>
        <input name="txlength" type="hidden" id="txlength" value="19" />
    </form>
    <script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["change"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "date", {validateOn:["change"], format:"yyyy-mm-dd"});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "integer", {validateOn:["blur", "change"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($Recordset1);
?>
