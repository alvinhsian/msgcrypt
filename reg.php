<?php require_once('Connections/localhost.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// *** Redirect if username exists
$MM_flag="MM_insert";
if (isset($_POST[$MM_flag])) {
  $MM_dupKeyRedirect="reg_error.php";
  $loginUsername = $_POST['email'];
  $LoginRS__query = sprintf("SELECT edit_key FROM `user` WHERE edit_key=%s", GetSQLValueString($loginUsername, "text"));
  mysql_select_db($database_localhost, $localhost);
  $LoginRS=mysql_query($LoginRS__query, $localhost) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);

  //if there is a row in the database, the username was found - can not add the requested username
  if($loginFoundUser){
    $MM_qsChar = "?";
    //append the username to the redirect page
    if (substr_count($MM_dupKeyRedirect,"?") >=1) $MM_qsChar = "&";
    $MM_dupKeyRedirect = $MM_dupKeyRedirect . $MM_qsChar ."requsername=".$loginUsername;
    header ("Location: $MM_dupKeyRedirect");
    exit;
  }
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form2")) {
  $insertSQL = sprintf("INSERT INTO `user` (pwd, email, random_var, edit_key) VALUES (%s, %s, %s, %s)",
                       //GetSQLValueString($_POST['uid'], "text"),
                       GetSQLValueString(hash('sha256',$_POST['pwd']), "text"),
                       GetSQLValueString($_POST['email'], "text"),
					   GetSQLValueString(randomKeyGenerator(), "text"),
					   GetSQLValueString(hash('sha256',$_POST['pwd'].randomKeyGenerator()), "text")
					   );

  mysql_select_db($database_localhost, $localhost);
  $Result1 = mysql_query($insertSQL, $localhost) or die(mysql_error());

  $insertGoTo = "login.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<?php 
				function randomKeyGenerator($pt=16, $myword=""){
					$randomKey="";
					$element="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
					$element.= $myword;
					$element_len=strlen($element);
					for($i=1;$i<=$pt;$i++){
						$rg=rand()%$element_len;
						$randomKey.=$element{$rg};
						}
						return $randomKey; 
					}//產生隨機亂數
			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>註冊新帳號</title>
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->註冊帳號<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <form name="form1" id="form1" method="POST" action="">
	   
    </form>
      <form action="<?php echo $editFormAction; ?>" method="post" id="form2">
        <table width="100%">
          <tr valign="baseline">
            <td align="right">電子郵件：</td>
            <td><span id="sprytextfield1">
            <label for="email"></label>
            <input name="email" type="text" id="email" size="40" />
            <span class="textfieldRequiredMsg">請輸入電子郵件</span><span class="textfieldInvalidFormatMsg">格式不正確</span></span></td>
          </tr>
          
          <tr valign="baseline">
            <td width="26%" align="right">密碼:</td>
            <td width="74%"><span id="sprypassword1">
            <input type="password" name="pwd" value="" size="40" id="pwd"/>
            <span class="passwordRequiredMsg">這是必要欄位</span><span class="passwordMinCharsMsg">密碼必須超過8位數</span></span></td>
          </tr>
          <tr valign="baseline">
            <td align="right">驗證密碼：</td>
            <td><span id="spryconfirm1">
              <input type="password" name="pwd2" value="" size="40" />
            <span class="confirmRequiredMsg">這是必要欄位</span><span class="confirmInvalidMsg">密碼不相符</span></span></td>
          </tr>
          <tr valign="baseline">
            <td align="right">&nbsp;</td>
            <td><input type="submit" value="免費註冊" /></td>
          </tr>
        </table>
        <input type="hidden" name="MM_insert" value="form2" />
    </form>
      <p>&nbsp;</p>
    <script type="text/javascript">
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {minChars:8});
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "pwd");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
