<?php require_once('Connections/localhost.php'); 
require_once('reCAPTCHA/recaptchalib.php');
$publickey = "6LdTcr4SAAAAAPBclgwBvjIqKQnmU50Iu6JzxUv3"; // you got this from the signup page
//echo recaptcha_get_html($publickey);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title></title>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'white'
 };
 </script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->遺失密碼<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <form id="form1" method="post" action="sendpasswd.php">
	    <table width="100%" border="0">
	      <tr>
	        <td width="30%" align="right">請填寫您的E-mail：</td>
	        <td width="70%"><label for="email"></label>
	          <span id="sprytextfield1">
              <input name="email" type="text" id="email" size="40" />
            <span class="textfieldRequiredMsg">請輸入您註冊時使用的信箱</span><span class="textfieldInvalidFormatMsg">格式不正確</span></span></td>
          </tr>
	      <tr>
	        <td align="right">請輸入圖形驗證碼：</td>
	        <td><?php echo recaptcha_get_html($publickey);?></td>
          </tr>
	      <tr>
	        <td align="right">&nbsp;</td>
	        <td><input type="submit" name="submit" id="submit" value="重設密碼" /></td>
          </tr>
        </table>
    </form>
	  <p>&nbsp;</p>
	  <p>&nbsp;</p>
    <script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
      </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
