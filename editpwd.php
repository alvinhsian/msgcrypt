<?php require_once('Connections/localhost.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE `user` SET pwd=%s WHERE edit_key=%s",
                       GetSQLValueString($_POST['pwd'], "text"),
                       GetSQLValueString(hash('sha256',$_POST['edit_key']), "text"));

  mysql_select_db($database_localhost, $localhost);
  $Result1 = mysql_query($updateSQL, $localhost) or die(mysql_error());

  $updateGoTo = "login.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset_pwd = "-1";
if (isset($_GET['edit_key'])) {
  $colname_Recordset_pwd = $_GET['edit_key'];
}
mysql_select_db($database_localhost, $localhost);
$query_Recordset_pwd = sprintf("SELECT pwd, random_var, edit_key FROM `user` WHERE edit_key = %s", GetSQLValueString($colname_Recordset_pwd, "text"));
$Recordset_pwd = mysql_query($query_Recordset_pwd, $localhost) or die(mysql_error());
$row_Recordset_pwd = mysql_fetch_assoc($Recordset_pwd);
$totalRows_Recordset_pwd = mysql_num_rows($Recordset_pwd);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title></title>
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->變更密碼<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <form name="form1" id="form1" method="POST" action="<?php echo $editFormAction; ?>">
	    <table width="100%" border="0">
	      <tr>
	        <td width="30%" align="right">請輸入新的密碼：</td>
	        <td width="70%"><span id="sprypassword1">
            <label for="pwd"></label>
            <input name="pwd" type="password" id="pwd" size="40" />
            <span class="passwordRequiredMsg">請輸入密碼</span><span class="passwordMinCharsMsg">密碼不得低於8個字元</span></span></td>
          </tr>
	      <tr>
	        <td align="right">再次確認新密碼：</td>
	        <td><span id="spryconfirm1">
	          <label for="repwd"></label>
	          <input name="repwd" type="password" id="repwd" size="40" />
            <span class="confirmRequiredMsg">請重複輸入密碼</span><span class="confirmInvalidMsg">密碼不相符</span></span></td>
          </tr>
	      <tr>
	        <td><input name="edit_key" type="hidden" id="edit_key" value="<?php echo $row_Recordset_pwd['edit_key']; ?>" /></td>
	        <td><input type="submit" name="submit" id="submit" value="變更密碼" />
            <input type="reset" name="reset" id="reset" value="重設" /></td>
          </tr>
        </table>
	    <input type="hidden" name="MM_update" value="form1" />
    </form>
    <script type="text/javascript">
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {minChars:8, validateOn:["blur"]});
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "pwd");
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($Recordset_pwd);
?>
