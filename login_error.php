<?php require_once('Connections/localhost.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['email'])) {
  $loginUsername=$_POST['email'];
  $password=hash('sha256',$_POST['pwd']);
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "setmsg.php";
  $MM_redirectLoginFailed = "login_error.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_localhost, $localhost);
  
  $LoginRS__query=sprintf("SELECT `email`, pwd FROM `user` WHERE `email`=%s AND pwd=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $localhost) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
	$getLoginTimes = "SELECT `login_times` FROM `user` WHERE email = '{$_POST['email']}'";
	$result_getLoginTimes = mysql_query($getLoginTimes);
	$getLoginTimes_resource = mysql_fetch_array($result_getLoginTimes);
	$loginTimes = $getLoginTimes_resource[0];
	$newLoginTimes = $loginTimes+1;	
	$updateLoginTime = "UPDATE `user` SET login_times = '{$newLoginTimes}' WHERE email = '{$_POST['email']}'";
	$result_updateLoginTime = mysql_query($updateLoginTime); //增加登入次數記錄
	
	$date_time=date("Y-m-d H:i:s");
	$updateLastLoginTime = "UPDATE `user` SET last_login_time = '{$date_time}' WHERE email = '{$_POST['email']}'";
	$result_updateLastLoginTime = mysql_query($updateLastLoginTime);
	//更新最後登入時間
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>登入錯誤</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>
<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->登入MsgCrypt<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->	
      <form id="form1" method="POST" action="<?php echo $loginFormAction; ?>">
      <div id="UIError"> 
        <h3>用戶名稱或密碼錯了</h3>
        <p> 查無與你所輸入的用戶名稱有關的帳號。請依照這些指示清除你的瀏覽器上的 cache 和 cookie。
          
          你可以使用登記在你帳號下的電郵地址登入。請確認輸入無誤。 </p>
      </div>
	    <table width="100%" border="0">
	      <tr>
	        <td width="30%" align="right">電子郵件：</td>
	        <td width="70%"><label for="email"></label>
            <input name="email" type="text" id="email" size="40" /></td>
          </tr>
	      <tr>
	        <td align="right">密碼：</td>
	        <td><label for="pwd"></label>
            <input name="pwd" type="password" id="pwd" size="40" /></td>
          </tr>
	      <tr>
	        <td>&nbsp;</td>
	        <td><input type="submit" name="submit" id="submit" value="登入" />
            <a href="forget_password.php">忘記密碼</a></td>
          </tr>
        </table>
    </form>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
