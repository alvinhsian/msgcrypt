<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>FAQ</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->FAQ<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <p>問：我要如何相信MsgCrypt絕對不會外洩儲存在資料庫中的敏感訊息？</p>
	  <div class="faqcontent">答：當你使用MsgCrypt加密訊息時，你的訊息會透過AES演算法加密。而我們的資料庫並不儲存您對訊息所設定的密碼。在沒有您的密碼情況之下任何人都無法將您的加密訊息破解，因此請您善盡保存密碼之責，千萬別遺失密碼。除此之外，當您的訊息超過允許閱讀此數或您設定的有效期限時，系統就會自動刪除您的加密訊息。</div>
	  <p>&nbsp;</p>
	  <p>問：萬一我將訊息錯發給不應該知道的人要怎麼辦？</p>
	  <div class="faqcontent">答：在MsgCrypt將您的訊息加密完畢後，系統會給您一個該訊息專屬的分享連結以及刪除連結。請您務必妥善保存這兩個重要連結！若遭遇錯發訊息時，您可以在很短的時間內於瀏覽器的網址列貼入訊息刪除連結將訊息刪除。</div>
	  <p>&nbsp;</p>
	  <p>問：MsgCrypt會突然向我收費嗎？</p>
	  <div class="faqcontent">答：我們必須坦白的告訴您所有的網路服務都需要維護成本，而目前為止MsgCrypt都是免費的，未來將會推出更進階的付費功能給VIP會員使用。而基本的加解密功能絕對不會收費的。</div>
	  <p>&nbsp; </p>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
