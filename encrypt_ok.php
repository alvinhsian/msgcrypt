<?php require_once('Connections/localhost.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_localhost, $localhost);
$query_Recordset_setting = "SELECT * FROM settings WHERE set_id = 1";
$Recordset_setting = mysql_query($query_Recordset_setting, $localhost) or die(mysql_error());
$row_Recordset_setting = mysql_fetch_assoc($Recordset_setting);
$totalRows_Recordset_setting = mysql_num_rows($Recordset_setting);

$colname_Recordset_msgkey = "-1";
if (isset($_GET['mid'])) {
  $colname_Recordset_msgkey = $_GET['mid'];
}
mysql_select_db($database_localhost, $localhost);
$query_Recordset_msgkey = sprintf("SELECT mid FROM msgdata WHERE mid = %s", GetSQLValueString($colname_Recordset_msgkey, "text"));
$Recordset_msgkey = mysql_query($query_Recordset_msgkey, $localhost) or die(mysql_error());
$row_Recordset_msgkey = mysql_fetch_assoc($Recordset_msgkey);
$totalRows_Recordset_msgkey = mysql_num_rows($Recordset_msgkey);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>加密完畢</title>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="jquery/jquery-1.7.2.min.js"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script type="text/javascript">
function copyToClipboard(txt) {
     if(window.clipboardData) {
             window.clipboardData.clearData();
             window.clipboardData.setData("Text", txt);
     } else if(navigator.userAgent.indexOf("Opera") != -1) {
          window.location = txt;
     } else if (window.netscape) {
          try {
               netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
          } catch (e) {
               alert("被瀏覽器拒絕！\n請在瀏覽器網址欄輸入'about:config'\n然後搜尋'signed.applets.codebase_principal_support'並設為'true'");
          }
          var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
          if (!clip)
               return;
          var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
          if (!trans)
               return;
          trans.addDataFlavor('text/unicode');
          var str = new Object();
          var len = new Object();
          var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
          var copytext = txt;
          str.data = copytext;
          trans.setTransferData("text/unicode",str,copytext.length*2);
          var clipid = Components.interfaces.nsIClipboard;
          if (!clip)
               return false;
          clip.setData(trans,null,clipid.kGlobalClipboard);
     }
     alert('已經複製到剪貼簿');
}
</script>

<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
	  <!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->訊息加密完畢！<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
    <h3>您的訊息已加密完畢！您可將以下連結分享給其他人。</h3>
    <table width="100%" border="0">
      <tr>
        <td width="20%" align="right">訊息分享連結：</td>
        <td width="80%"><input name="link1" type="text" id="link1" value="<?php echo "http://".$row_Recordset_setting['set_var']."verifymsg.php?mid=".$row_Recordset_msgkey['mid'] ;?>" size="70" readonly="readonly" onclick="copyToClipboard(link1.value);"/>
        <input type="button" name="copy1" id="copy1" value="複製" onclick="copyToClipboard(link1.value);"/></td>
      </tr>
      <tr>
        <td align="right">訊息刪除連結：</td>
        <td><input name="link2" type="text" id="link2" value="<?php echo "http://".$row_Recordset_setting['set_var']."deletemsg.php?mid=".$row_Recordset_msgkey['mid'] ;?>" size="70" readonly="readonly" onclick="copyToClipboard(link2.value);"/>
        <input type="button" name="copy2" id="copy2" value="複製" onclick="copyToClipboard(link2.value);"/></td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <p>
      <label for="link1"></label>
    </p>
	  <h3>或以電子郵件的方式<strong>(不含刪除連結)</strong>寄給特定人士：</h3>
	  <form id="form1" method="post" action="sendmail.php">
	        <table width="100%" border="0">
	          <tr>
	            <td width="20%" align="right">寄件人姓名：</td>
	            <td width="80%"><span id="sprytextfield2">
	              <label for="name"></label>
	              <input name="name" type="text" id="name" size="40" />
                <span class="textfieldRequiredMsg">不可以寄匿名信</span></span></td>	            
              </tr>
	          <tr>
	            <td align="right">收件人E-mail：</td>
	            <td><span id="sprytextfield1">
                <label for="msgpwd">
                  <input name="email" type="text" id="email" size="40" />
                </label>
                <span class="textfieldRequiredMsg">請輸入收件人E-mail</span><span class="textfieldInvalidFormatMsg">無效的格式</span></span></td>
              </tr>
	          <tr>
	            <td align="right">傳送訊息密碼：</td>
	            <td align=><label>
	              <input name="RadioGroup_msgpwd" type="radio" id="RadioGroup_msgpwd_0" onclick="msgpwd.disabled=true" value="0" checked="checked"/>
	              向我詢問</label>
                  <label>
                    <input type="radio" name="RadioGroup_msgpwd" value="1" id="RadioGroup_msgpwd_1" onclick="msgpwd.disabled=false"/>
                    傳送密碼<br />
                  </label>
                <input name="msgpwd" type="password" disabled="disabled" id="msgpwd" size="40" /></td>
              </tr>
	          <tr>
	            <td align="right"><input name="link1" type="hidden" id="link1" value="<?php echo "http://".$row_Recordset_setting['set_var']."verifymsg.php?mid=".$row_Recordset_msgkey['mid'] ;?>" />	              <input name="mid" type="hidden" id="mid" value="<?php echo $row_Recordset_msgkey['mid']; ?>" /></td>
	            <td align=><input type="submit" name="submit" id="submit" value="傳送訊息" />
                <input type="reset" name="reset" id="reset" value="清除" /></td>
              </tr>
        </table>    
    </form>
    <script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($Recordset_setting);

mysql_free_result($Recordset_msgkey);
?>
