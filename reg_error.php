<?php require_once('Connections/localhost.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// *** Redirect if username exists
$MM_flag="MM_insert";
if (isset($_POST[$MM_flag])) {
  $MM_dupKeyRedirect="reg_error.php";
  $loginUsername = $_POST['email'];
  $LoginRS__query = sprintf("SELECT random_var FROM `user` WHERE random_var=%s", GetSQLValueString($loginUsername, "text"));
  mysql_select_db($database_localhost, $localhost);
  $LoginRS=mysql_query($LoginRS__query, $localhost) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);

  //if there is a row in the database, the username was found - can not add the requested username
  if($loginFoundUser){
    $MM_qsChar = "?";
    //append the username to the redirect page
    if (substr_count($MM_dupKeyRedirect,"?") >=1) $MM_qsChar = "&";
    $MM_dupKeyRedirect = $MM_dupKeyRedirect . $MM_qsChar ."requsername=".$loginUsername;
    header ("Location: $MM_dupKeyRedirect");
    exit;
  }
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO `user` (pwd, email, random_var, edit_key) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString(hash('sha256',$_POST['pwd']), "text"),
                       GetSQLValueString($_POST['email'], "text"),
					   GetSQLValueString(randomKeyGenerator(), "text"),
					   GetSQLValueString(hash('sha256',$_POST['pwd'].randomKeyGenerator()), "text"));

  mysql_select_db($database_localhost, $localhost);
  $Result1 = mysql_query($insertSQL, $localhost) or die(mysql_error());

  $insertGoTo = "login.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

function randomKeyGenerator($pt=16, $myword=""){
					$randomKey="";
					$element="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
					$element.= $myword;
					$element_len=strlen($element);
					for($i=1;$i<=$pt;$i++){
						$rg=rand()%$element_len;
						$randomKey.=$element{$rg};
						}
						return $randomKey; 
					}//產生隨機亂數
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html><!-- InstanceBegin template="/Templates/theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>錯誤</title>
<link href="SpryAssets/SpryValidationConfirm.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationPassword.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="theme/dark_theme/images/styles.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<script src="SpryAssets/SpryValidationConfirm.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationPassword.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<div id="HEADER">
	<h1>MsgCrypt-Deciding who can read</h1>
	<ul>
	  <!-- InstanceBeginEditable name="EditRegion3" -->EditRegion3
	  <li><a href="#">ContactUs</a></li>
		<li><a href="#">Sign Up</a></li>
		<li><a href="#">Sign In</a></li>
		<li><a href="#">News</a></li>
		<li><a href="#">Home</a></li>
		<!-- InstanceEndEditable -->
	</ul>
	<div class="Visual"> </div>
</div>

<div id="CONTENT">
	<h2><!-- InstanceBeginEditable name="EditRegion2" -->註冊新帳號<!-- InstanceEndEditable --></h2>
	<div id="TEXT"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <div id="UIError">
      <h3>您輸入的電子郵件已經有人使用</h3>
      <p>您輸入的電子郵件已經被其他人註冊。如果您是該帳號的主人請<a href="login.php">直接登入</a>或申請<a href="forget_password.php">重新設定密碼</a>。</p>
	  </div>
    <form name="form1" id="form1" method="POST" action="<?php echo $editFormAction; ?>">
      <table width="100%" border="0">
        <tr>
          <td width="30%" align="right">電子郵件：</td>
          <td width="70%"><label for="email"></label>
            <span id="sprytextfield1">
            <input name="email" type="text" id="email" size="40" />
          <span class="textfieldRequiredMsg">請輸入電子郵件</span><span class="textfieldInvalidFormatMsg">無效的格式</span></span></td>
        </tr>
        <tr>
          <td align="right">密碼：</td>
          <td><label for="pwd"></label>
            <span id="sprypassword1">
            <input name="pwd" type="password" id="pwd" size="40" />
          <span class="passwordRequiredMsg">請輸入密碼</span><span class="passwordInvalidStrengthMsg">密碼至少需要8個字</span></span></td>
        </tr>
        <tr>
          <td align="right">重新輸入密碼：</td>
          <td><label for="repwd"></label>
            <span id="spryconfirm1">
            <input name="repwd" type="password" id="repwd" size="40" />
          <span class="confirmRequiredMsg">請重複輸入密碼</span><span class="confirmInvalidMsg">密碼不相符</span></span></td>
        </tr>
      </table>
      <div align="center">
        <input type="submit" name="submit" id="submit" value="註冊帳號" />
      </div>
      <input type="hidden" name="MM_insert" value="form1" />
    </form>
    <script type="text/javascript">
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "pwd");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "email");
var sprypassword1 = new Spry.Widget.ValidationPassword("sprypassword1", {minAlphaChars:8});
    </script>
	<!-- InstanceEndEditable -->		
	</div>
</div>

<div id="FOOTER">
<p><a href="#">FAQ</a> &bull; <a href="#">Terms</a> &bull; <a href="#">Privacy Policy</a> &bull; <a href="#">About Us</a></p>
<p>Msg Crypt &copy; 2012 </p>

</div>

</body>
<!-- InstanceEnd --></html>
